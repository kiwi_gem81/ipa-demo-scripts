#!/bin/bash

# Script must be run as root. 
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

RNAME="VM-MyNewVM-1"

virt-install --name $RNAME \
             --ram 1024 \
             --disk path=/storage/virt/data/$RNAME.img,size=10,size=10 \
	     --network bridge:br1 \
	     --vnc \
	     --os-type=linux \
	     --os-variant=rhel6 \
	     --location http://ucmirror.canterbury.ac.nz/linux/centos/6/os/x86_64/ \
	     --initrd-inject=/root/git/ipa-demo-scripts/ipaserver1-ks.cfg \
	     --extra-args="ks=file:/ipaserver1-ks.cfg" \
 	     --noautoconsole
