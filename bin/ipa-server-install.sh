#!/bin/bash

# Script must be run as root. 
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# If firewall rules haven't been made this can serioulsy break the installed
# Lets just disable iptables for now. 
service iptables stop

# This script will build the ipa-server-master
ipa-server-install -a passw0rd\
                   --hostname=ipaserver1.demo.lab \
                   -n demo.lab \
	  	   -p passw0rd \
		   -P passw0rd \
		   -r DEMO.LAB \
		   --setup-dns \
  		   --no-forwarders

